import PySimpleGUI as sg 
import pygit2
from pygit2 import Repository

class popup:
    params_layout = [
        [sg.Text("Comment utiliser ce logiciel :")],
        [sg.Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")],
        [sg.Text(" Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.")],
        [sg.Text(" Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.")],
        [sg.Text( " Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")]
        ]
        
    def open_window(self):
        layout = [[sg.Frame("aide",self.params_layout)],[sg.Button("Mise à jour",key="update")]]
        window = sg.Window("Aide", layout, modal=True)
        choice = None
        while True:
            event, values = window.read()
            if event == "Exit" or event == sg.WIN_CLOSED:
                break
            elif event =="update":
                confirm=popupConfirm()
                confirm.open_window()
        window.close()
        
class popupConfirm():

    def pull(self,repo, remote_name='origin', branch='master'):
        for remote in repo.remotes:
            if remote.name == remote_name:
                remote.fetch()
                remote_master_id = repo.lookup_reference('refs/remotes/origin/%s' % (branch)).target
                merge_result, _ = repo.merge_analysis(remote_master_id)
                # Up to date, do nothing
                if merge_result & pygit2.GIT_MERGE_ANALYSIS_UP_TO_DATE:
                    return
                # We can just fastforward
                elif merge_result & pygit2.GIT_MERGE_ANALYSIS_FASTFORWARD:
                    repo.checkout_tree(repo.get(remote_master_id))
                    try:
                        master_ref = repo.lookup_reference('refs/heads/%s' % (branch))
                        master_ref.set_target(remote_master_id)
                    except KeyError:
                        repo.create_branch(branch, repo.get(remote_master_id))
                    repo.head.set_target(remote_master_id)
                elif merge_result & pygit2.GIT_MERGE_ANALYSIS_NORMAL:
                    repo.merge(remote_master_id)

                    if repo.index.conflicts is not None:
                        for conflict in repo.index.conflicts:
                            print('Conflicts found in:', conflict[0].path)
                        raise AssertionError('Conflicts, ahhhhh!!')

                    user = repo.default_signature
                    tree = repo.index.write_tree()
                    commit = repo.create_commit('HEAD',
                                                user,
                                                user,
                                                'Merge!',
                                                tree,
                                                [repo.head.target, remote_master_id])
                    # We need to do this or git CLI will think we are still merging.
                    repo.state_cleanup()
                else:
                    raise AssertionError('Unknown merge analysis result')
                
    def proceed_update(self):
        #pygit.repos()
        #print("Update Git")
        #r = pygit.load("radioactivite")
        #print(r)
        #print(r.fetch())
        #print(r.status())
        #print(r.pull())
        #print("Done")
        repo = Repository('../.git')
        #print(repo.remotes["origin"].fetch())
        # print(repo.merge())
        self.pull(repo)

        
    def open_window(self):
        layout = [[sg.Text("Cette opération doit être réalisée à la demande d'un développeur. Etes vous sur de vouloir continuer?", key="new")],[sg.Button("Oui"),sg.Button("Non")]]
        window = sg.Window("Aide", layout, modal=True)
        choice = None
        while True:
            event, values = window.read()
            if event == "Exit" or event == sg.WIN_CLOSED:
                break
            elif event =="Oui":
                self.proceed_update()
                window.close()
            elif event =="Non" :
                window.close()
            
        window.close()