
IF NOT EXIST "%userprofile%\bin" ( mkdir "%userprofile%\bin" )

cd %userprofile%\bin

IF EXIST "radioactivite" ( cd radioactivite & git pull ) ELSE ( git clone "https://gitlab.com/zerorist/radioactivite" )


cd radioactivite
copy ObsGui\ObsGui.lnk %UserProfile%\Desktop\

pip install obs-websocket-py pysimplegui clipboard psutil pillow pygit2
