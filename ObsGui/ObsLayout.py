from obswebsocket import obsws, requests,events
from ObsSrc import ObsSrc
import os
import datetime

class ObsLayout:
    def __init__(self,ws):
        self.ws = ws
        response = self.ws.call(requests.GetVideoInfo())
        self.width = response.getBaseWidth()
        self.height = response.getBaseHeight()
        self.title = ""
        
    async def updateBgImage(self,file):
        settings = {}
        settings['file'] = file

        response = self.ws.call(requests.SetSourceSettings(sourceName="bgImage",sourceType="image_source",sourceSettings=settings))
                
        if response.status:
            response = self.ws.call(requests.GetSceneItemProperties(item="bgImage"))
            w = response.getSourceWidth()
            h = response.getSourceHeight()

            print("{},{}/{},{}".format(w,h,self.width,self.height))
            position={}
            position['alignement'] = 5
            #position['x'] = (self.width-w)/2
            #position['y'] = (self.height-h)/2
            
            scalex = self.width/w
            scaley = self.height/h
            
            if scalex < scaley :
                scaley = scalex    
            else:
                scalex = scaley
                
            position['x'] = (self.width-w*scalex)/2
            position['y'] = (self.height-h*scalex)/2
                
            scale={}
            scale['x'] = scalex
            scale['y'] = scalex
            
            response = self.ws.call(requests.SetSceneItemProperties(item="bgImage",position=position,scale=scale))
        
            if not response.status:
                print("UpdateBgImage error! Reason:", response)
        else:
            print("Couldn't set bgimage settings! Reason:", response.data)
            
    async def updateTitle(self,title):
        #scene = getCurrentSceneName(ws)
        settings = {}
        settings['text'] = title

        response = self.ws.call(requests.SetSourceSettings(sourceName="titleBox",sourceType="text_gdiplus_v2",sourceSettings=settings))
                
        if not response.status:
            print("UpdateTitle error! Reason:", response)
        else:
            self.updateTitlePosition()
            
    def getCurrentSceneName(self):
        response = self.ws.call(requests.GetCurrentScene())
        if response.status:
            return response.getName()
        else:
            return "NoSceneFound"

    def updateTitlePosition(self):
        response = self.ws.call(requests.GetSceneItemProperties(item="titleBox"))
        w = response.getSourceWidth()
        h = response.getSourceHeight()

        position={}
        #position['alignement'] = 5
        position['x'] = (self.width-w)/2
        position['y'] = 0#(self.height-h)
        response = self.ws.call(requests.SetSceneItemProperties(item="titleBox",position=position))
   

        if not response.status:
            print("titleBox setsettings error! Reason:", response)
            
                        
    async def initScene(self):
        # CHECK SCENE
        ###############
        if self.getCurrentSceneName() == "radioactivité":
            print ("scene radioactivité already in use")
        else :
            response = self.ws.call(requests.GetSceneList())
            already_exists = False
        
            if response.status :
                for scn in response.getScenes():
                    if scn['name'] == "radioactivité":
                            already_exists = True
                            
                if not already_exists :
                    response = self.ws.call(requests.CreateScene(sceneName="radioactivité"))
                    print ("scene radioactivité  created")

                response = self.ws.call(requests.SetCurrentScene(scene_name="radioactivité"))
                print ("scene radioactivité called")
        scene="radioactivité"
        
        sourcesInfos = {}
        sourcesInfos['bgColor'] = ObsSrc('bgColor',"color_source_v3")
        sourcesInfos['bgImage'] =  ObsSrc('bgImage',"image_source")
        sourcesInfos['titleBox'] =  ObsSrc('titleBox',"text_gdiplus_v2")
        sourcesInfos['bgTitle'] =  ObsSrc('bgTitle',"color_source_v3")
        sourcesInfos['spectre'] =  ObsSrc('spectre',"spectralizer")

        
        settings = {}
        date = datetime.date.today()
        settings['text'] = 'Emission du {}'.format(date)
        settings['align'] = 'right'
        settings['font'] = {'face' : 'Bahnschrift','size':120}
#        settings['font']['face'] = 'Bahnschrift'
#        settings['font']['size'] = '72'
#        settings['height'] = '72'
        sourcesInfos['titleBox'].setSettings(settings)
        
        settings = {}
        settings['file']=os.path.join(os.path.dirname(os.path.abspath(__file__)),"radioactivite.png")
        sourcesInfos['bgImage'].setSettings(settings)

        settings = {}
        settings['color'] = 1686077311;
        settings['width'] =1280
        settings['height']=100
        sourcesInfos['bgTitle'].setSettings(settings)
        
        settings = {}
        settings['width']=1280
        settings['height']=720
        sourcesInfos['bgColor'].setSettings(settings)


        #settings = {'audio_source': 'Audio du Bureau', 'filter_mode': 0, 'filter_strength': 1.5, 'gravity': 0.3, 'height': 250, 'source_mode': 0}
        #{'audio_source': 'Mic/Aux', 'bar_space': 1, 'color': 4286556757, 'corner_radius': 4, 'detail': 50, 'filter_mode': 0, 'filter_strength': 1.5, 'gravity': 0.04, 'height': 180, 'log_freq_scale': False, 'log_freq_scale_start': 28.0, 'log_freq_scale_use_hpf': False, 'round_corners': False, 'source_mode': 2, 'use_auto_scale': True, 'wire_mode': 1, 'wire_thickness': 2}
        settings = {'position': {'alignment': 5, 'x': 0.0, 'y': 720.0},'audio_source': 'Mic/Aux', 'bar_space': 1, 'color': 4286089210, 'corner_radius': 4, 'detail': 215, 'filter_mode': 0, 'filter_strength': 1.5, 'gravity': 0.5, 'height': 180, 'log_freq_scale': False, 'log_freq_scale_start': 28.0, 'round_corners': True, 'source_mode': 2, 'wire_mode': 2}
        sourcesInfos['spectre'].setSettings(settings)
        
        sceneItemProperties = {}
        sceneItemProperties['item']="spectre"
        sceneItemProperties['position']={'x':0,'y':720}
        response = self.ws.call(requests.GetSceneItemList(sceneName=scene))
        already_exists = False
        
        #recherche des sources existantes dans OBS
        if response.status :
            for src in response.getSceneItems():
                if src['sourceName'] == "bgColor" or  src['sourceName'] == "bgImage" or  src['sourceName'] == "titleBox" or src['sourceName'] == "bgTitle" or src['sourceName'] == "spectre" :
                    sourcesInfos[src['sourceName']].foundInObs()
                    sourcesInfos[src['sourceName']].setId(src['itemId'])
                    sourcesInfos[src['sourceName']].setProperties(self.ws.call(requests.GetSceneItemProperties(item=src['sourceName'])))
                    print("SOURCE FOUND : {}".format(src))

        for src in sourcesInfos.values():
            print("******************************{}************************************".format(src.getName()))
            if src.existsInObs():
                #récupération des infos
                response = self.ws.call(requests.GetSourceSettings(sourceName=src.getName()))
                if src.getName() == "titleBox":
                    self.title = response.getSourceSettings()['text']
                print("Source settings found : {}".format(response.getSourceSettings()))
                self.ws.call(requests.SetSceneItemProperties(item=src.getName(),visible=True))
                
            else :
                #création de la source
                print("Source {} does not exist, creation".format(src.getName()))
                response = self.ws.call(requests.CreateSource(sourceName=src.getName(),sourceKind=src.getType(),sceneName=scene,sourceSettings=src.getSettings()))
                src.setId(response.getItemId())
                print ("Source {} created : {}".format(src.getName(),response))
                if src.getName() == "titleBox":
                    #response = self.ws.call(requests.SetTextGDIPlusProperties(source="titleBox",align="right",font=src.getSettings()['font'],bk_color=4291935684,bk_opacity=50))
                    response = self.ws.call(requests.SetTextGDIPlusProperties(source="titleBox",align="right",font=src.getSettings()['font'],bk_color=0,bk_opacity=100))
                    self.title = src.getSetting("text")
                elif src.getName() == "bgColor":
                
                    #print("###########BGCOLOR###################")
                    #print(src.getSettings())
                    position={}
                    position['alignement'] = 5
                    scale={}
                    #scale['x'] = w/bgw
                    #scale['y'] = h/bgh
                    response = self.ws.call(requests.SetSceneItemProperties(item="bgColor",position=position,scale=scale,visible=True))                    
                elif src.getName() == "bgImage":
                    await self.updateBgImage(os.path.join(os.path.dirname(os.path.abspath(__file__)),"radioactivite.png"))
                    position={}
                    position['alignement'] = 5
                    response = self.ws.call(requests.SetSceneItemProperties(item="bgImage",position=position,visible=True))
                elif src.getName() == "bgTitle":
                    position={}
                    position['alignement'] = 5
                    #scale={}
                    #scale['x'] = 4
                    #scale['y'] = 0.5
                    response = self.ws.call(requests.SetSceneItemProperties(item="bgTitle",position=position,visible=True))
                elif src.getName() == "spectre":
                    position={}
                    position['alignement'] = 5
                    position['x'] = 0
                    position['y'] = 540
                    scale = {'x':1,'y':1}
                    response = self.ws.call(requests.SetSceneItemProperties(item="spectre",scale=scale,position=position,visible=True))
                    
        items = [{'id':sourcesInfos['titleBox'].getId(),'name':sourcesInfos['titleBox'].getName()},
                 {'id':sourcesInfos['spectre'].getId(),'name':sourcesInfos['spectre'].getName()},
                 {'id':sourcesInfos['bgTitle'].getId(),'name':sourcesInfos['bgTitle'].getName()},
                 {'id':sourcesInfos['bgImage'].getId(),'name':sourcesInfos['bgImage'].getName()},
                 {'id':sourcesInfos['bgColor'].getId(),'name':sourcesInfos['bgColor'].getName()}]
        
        self.updateTitlePosition()
        
        response = self.ws.call(requests.ReorderSceneItems(items=items))
        print ("Reorder response : {}".format(response))
