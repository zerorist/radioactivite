class ObsSrc:
    exists = False
    settings = {}
     
    def __init__(self,name,srctype):
        self.name = name
        self.srctype = srctype
        
    def foundInObs(self):
        self.exists = True
        
    def existsInObs(self):
        return self.exists
        
    def setId(self,id):
        self.id = id
    
    def getId(self):
        return self.id
        
    def getName(self):
        return self.name
        
    def getType(self):
        return self.srctype
    
    def setSettings(self,settings):
        self.settings = settings
        
    def getSettings(self):
        return self.settings
    
    def getSetting(self,settingName):
        return self.settings[settingName]
        
    def setProperties(self,properties):
        self.properties = properties
        
    def getProperties(self):
        return self.properties
        
        