# Installer python [https://www.python.org/]
#installer OBS [https://obsproject.com/] + plugin websocket |https://github.com/Palakis/obs-websocket/releases]
# pip install obs-websocket-py pysimplegui clipboard psutil pillow pygit2
# git clone https://gitlab.com/zerorist/radioactivite.git
# python -m pygit --m C:\Users\user\Documents -v 1
# git config --global user.email "juzam@webmails.com"
# git config --global user.name "zerorist"


import PySimpleGUI as sg 
import tkinter as tk
from PIL import Image, ImageTk

import os
import subprocess
import clipboard
import logging
import sys
import asyncio
import psutil
import io
import webbrowser
import tempfile
import time

from ObsLayout import ObsLayout

from ObsSrc import ObsSrc
from popup import popup

from obswebsocket import obsws, requests,events
from tempfile import NamedTemporaryFile

updateScreenshot = True
getSourceInfo = ""
restartObs = False

def startUniqueObs():
    '''
    Check if OBS studio process is active
    '''
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if "obs64.exe" in proc.name().lower():
                return
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
   
    subprocess.Popen(r'C:\Program Files\obs-studio\bin\64bit\obs64.exe --minimize-to-tray', cwd=r'C:\Program Files\obs-studio\bin\64bit')

def get_img_data(f, maxsize=(400, 400), first=False):
    '''
    Generate image data using PIL
    '''
    img = Image.open(f)
    img.thumbnail(maxsize)
    if first:                     # tkinter is inactive the first time
        bio = io.BytesIO()
        img.save(bio, format="PNG")
        del img
        return bio.getvalue()
    return ImageTk.PhotoImage(img)

def on_event(event):
    global updateScreenshot,getSourceInfo,restartObs
    
    print("Got OBS event : {}".format(event.name))
    #WS EVENTS
    if event.name == "StreamStopping" :
        print(u"Event : Stream is stopping!")
    elif event.name == "StreamStopped" :
        print(u"Event : Stream has stopped!")
    elif event.name == "StreamStatus" :
        print(u"Event : Stream status!")
    elif event.name == "StreamStarting" :
        print(u"Event : Stream is starting!!")
    elif event.name == "StreamStarted" :
        print(u"Event : Stream is started!!")
    elif event.name == "SceneItemTransformChanged" :
        print("SceneItemTransformChanged : {}".format(event))
        #width = event.getTransform()['sourceHeight']
        #height = event.getTransform()['sourceWidth']
        #if not event.getItemName() == "titleBox":
        #    print("Updating screenshot!")
        updateScreenshot = True

    # WARNING LOOOOOOOOPPPPPP
    
    elif  event.name == "SceneItemSelected" :
        getSourceInfo = event.getItemName()
    elif event.name == "Exiting" :
        print(u"OBS STUDIO CLOSED!")
        restartObs = True
    else :    
        print(u"Event :{}".format(event))


async def startStream(ws,token):
    '''
    Start Obs stream via websocket
    '''
    s = {}
    s['server'] = "rtmps://live-api-s.facebook.com:443/rtmp/"
    s['key'] = token
  
    response = ws.call(requests.SetStreamSettings(type='rtmp_custom',settings=s,save=True))
    print(response.data)

    stream = {}
    stream['type'] = 'rtmp_custom'
    stream['settings'] = s

    response = ws.call(requests.StartStreaming(stream=stream))
    if response.status:
        print("Streaming has started")
    else:
        print("Couldn't start the stream! Reason:", response.data)

async def stopStream(ws):
    '''
    Stop Obs stream via websocket
    '''
    response = ws.call(requests.StopStreaming())
    if response.status:
        print("Streaming has stopped")
    else:
        print("Couldn't stop the stream! Reason:", response.data)
        
async def loadScreenshot(ws,fname):
    response = ws.call(requests.TakeSourceScreenshot(saveToFilePath=fname,fileFormat="png"))
    if response.status:
        print("Screenshot ok")
    else:
        print("Request error! Reason:", response.data)

      
async def showSourceList(ws):
    response = ws.call(requests.GetSourcesList())

    if response.status:
        print("GetSourcesList ok : {}".format(response.getSources()))
    else:
        print("GetSourcesList error! Reason:", response.data)

async def testRequest(ws):
    print("")
    #global updateScreenshot
    #updateScreenshot = True

    #response = ws.call(requests.GetSceneItemProperties(item="bgImage"))
    #w = response.getSourceWidth()
    #h = response.getSourceHeight()

    #if response.status:
    #    print("Test Request ok : {}".format(response))
    #else:
    #    print("Test Request error! Reason:", response)
   
async def main():
    global updateScreenshot,getSourceInfo,restartObs

    ws = obsws('localhost', 4444, "password")
    ws.register(on_event)
    ws.connect()

    obslayout = ObsLayout(ws)
    await obslayout.initScene()
    updateTitleFromObs = True
    updateTitleToObs = False
    pp = popup()
    updateScreenshot = True
    lastTs = 0
    lastTs2 = 0
    
    while True: #loop event simplegui
        event, values = window.read(timeout=200)
        #print("New event :{},{}".format(event,values)) 
        
        ts = time.time()
        if updateScreenshot and ts > lastTs + 0.5:
            lastTs = ts
            updateScreenshot = False
            with NamedTemporaryFile(suffix=".png") as tmpFile :
                file = tmpFile.name
                tmpFile.close()
                print("taking screenshot to {}".format(file))
                await loadScreenshot(ws,file)
                window.FindElement('img').update(data=get_img_data(file, first=True))
                #tmpFile.close()
                os.remove(file)
                
        if getSourceInfo != "":
            response = ws.call(requests.GetSourceSettings(sourceName=getSourceInfo))
            getSourceInfo=""

            if response.status:
                print("getSourceInfo ok : {}".format(response.getSourceSettings()))
            else:
                print("getSourceInfo error! Reason:", response.data)
                
        if restartObs :
            restartObs = False
            print("Restarting Obs")
            startUniqueObs()
            
        if updateTitleFromObs: #tention c'est le title coté gui
            print("Update title from obs")
            updateTitleFromObs = False
            window['title'].Update(obslayout.title)
        
        if updateTitleToObs and ts > lastTs2 + 1:
            print("updating title to Obs")
            lastTs2 = ts
            updateTitleToObs = False
            await obslayout.updateTitle(values['title'])
            
           
        if event == sg.WIN_CLOSED:
            await stopStream(ws)
            break
        elif event == "__TIMEOUT__":
            #print("Trapped fk timeout")
            continue
        elif event == "title":
            #print("got title event")
            updateTitleToObs = True
        elif event == "Paste":     
            window['FBK'].Update(clipboard.paste())
        elif event == "Test Request":
            await(testRequest(ws))
        elif event == "_FILEBROWSE_":
            print(values['_FILEBROWSE_'] )
            await obslayout.updateBgImage(values['_FILEBROWSE_'])
        elif event == "StartStream":
            #loop.run_until_complete(startStream(obsws,window['FBK'].Get()))
            await startStream(ws,window['FBK'].Get())
            print(window['FBK'].Get())

        elif event == "StopStream":
            await stopStream(ws)
        elif event == "GetKey":
            webbrowser.open('https://www.facebook.com/live/producer?ref=OBS')
        elif event == "Help":
            pp.open_window()
 
        else :
            print("Unused event :{},{}".format(event,values)) 

#loop = asyncio.get_event_loop()

startUniqueObs()
time.sleep(10)
font = ("Helvetica", 12)
sg.theme('DarkBlue3')

#canvas_elem = sg.Canvas(size=(350, 350), key='canvas')
img_elem = sg.Image(key="img")
file_types=(("Images", "*.jpg;*.jpeg;*.png;*.gif"),)

params_layout = [
        [sg.Text("Name :")],
        [sg.InputText(key='title',enable_events=True)],
        [sg.Text("Image :")],
        [sg.Input(key='_FILEBROWSE_', enable_events=True, visible=False),sg.FileBrowse("File",target='_FILEBROWSE_',file_types=file_types)],
        [sg.Text("Facebook stream key :")],
        [sg.InputText(key='FBK'),sg.Button("Get key",key="GetKey"),sg.Button("Paste",key="Paste")]
        ]

layout = [ 
    [img_elem,sg.Frame('', params_layout, font='Any 12', title_color='blue')], 
    [sg.Button("Go live", key="StartStream"),sg.Button("End live", key="StopStream"),sg.Button("Help",key="Help")] 
] 
  
window = sg.Window('DSF Quick Streamer', layout)
asyncio.run(main())
window.close()
